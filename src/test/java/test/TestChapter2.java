package test;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.Chapter2;
import pages.HomePage;

/**
 *
 * @author rene.alfaro
 */
public class TestChapter2 {
    private WebDriver selenium;
    
    public TestChapter2() {
        
    }
    
    @Before
    public void setUp() {
        selenium = new ChromeDriver();
        selenium.manage().window().maximize();
    }
    
    @After
    public void tearDown() {
        selenium.quit();
    }

    @Test
    public void shouldCheckAnotherButtonOnChapter2Page(){
        selenium.get("http://book.theautomatedtester.co.uk");
        HomePage hp = new HomePage(selenium);
        Chapter2 ch = hp.clickChapter2();
        assertTrue(ch.isButtonPresent("but1"));
    }
}
