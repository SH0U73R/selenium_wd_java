package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {
    
    WebDriver selenium;
    public HomePage(WebDriver selenium){
        this.selenium = selenium;
    }
    
    public Chapter2 clickChapter2(){
        clickChapter2("2");
        /* 
            Returns an instance of Chapter2 class as a result in order to render
            a new WebDriver object.
        */
        return new Chapter2(selenium);
    }

    // Searchs for chapter2 link text.
    private void clickChapter2(String number) {
        selenium.findElement(By.linkText("Chapter"+number)).click();
    }
}
