package pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Chapter2 {
    WebDriver selenium;
    
    //Receive a WebDriver object when an instance of this class is created.
    public Chapter2(WebDriver selenium) {
        this.selenium = selenium;
        if(!"Chapter 2".equalsIgnoreCase(this.selenium.getTitle())){
            selenium.get("http://book.theautomatedtester.co.uk/chapter2");
        }
    }
    
    public boolean isButtonPresent(String button){
        return selenium.findElements(By.xpath("//input[@id='" + button +"']")
        ).size()>0;
    }
    
}
